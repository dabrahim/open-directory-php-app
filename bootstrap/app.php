<?php

/**
 * @author Ibrahima Ndiaye
 */

ini_set("soap.wsdl_cache_enabled", 0);

require_once __DIR__ . '/../vendor/autoload.php';

//LOADING .env file
$dotenv = Dotenv\Dotenv::create(__DIR__ . '/../');
$dotenv->load();