$(function () {

    $('#add-user-form').on('submit', function (e) {
        e.preventDefault();

        const formData = new FormData(e.target);

        $.ajax({
            method: 'POST',
            url: '/api/users',
            data: formData,
            success: function (response) {
                if (response.success) {
                    const user = response.data;
                    appendUser(user);
                    e.target.reset();
                }
            },
            error: function () {
                alert('Un unknown error occurred. Please retry later.');
            },
            complete: hideLayer,
            processData: false,
            contentType: false
        });
    })

    $('#update-user-form').on('submit', function (e) {
        e.preventDefault();

        const id = $(e.target).find('input[name="id"]').val();
        const firstName = $(e.target).find('input[name="firstName"]').val();
        const lastName = $(e.target).find('input[name="lastName"]').val();
        const login = $(e.target).find('input[name="login"]').val();
        const password = $(e.target).find('input[name="password"]').val();

        $.ajax({
            method: 'PUT',
            url: '/api/users/' + id,
            data: {firstName, lastName, login, password},
            success: function (response) {
                const tr = $('#user-' + id);
                const tdList = $(tr).children();

                tdList[1].innerText = response.firstName;
                tdList[2].innerText = response.lastName;
                tdList[4].innerText = response.login;

                hideLayer();
            },
            error: function () {
            }
        })
    })
});