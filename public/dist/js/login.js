$(function () {
    //LOGIN
    $('#login-form').on('submit', function (e) {
        e.preventDefault();

        const formData = new FormData(e.target);

        $.ajax({
            url: '/api/users/login',
            method: 'POST',
            data: formData,
            success: function (response) {
                if (response.success) {
                    //display message
                    window.location = response.redirectTo;

                } else {
                    alert(response.error.message);
                }
            },
            error: function (message) {
                alert('An unknown error occurred. Please retry later.')
            },
            processData: false,
            contentType: false
        })
    });
});