function editUser(ref, id) {
    const _trRef = $(ref).closest('tr');
    const _trChildren = $(_trRef).children();

    const firstName = _trChildren[1].innerText;
    const lastName = _trChildren[2].innerText;
    const login = _trChildren[4].innerText;

    //KIK FIX
    $('#flag-form').remove();

    const userId = $('#flag').data('id');

    if (parseInt(userId) === parseInt(id)) {
        $('#update-user-form button[type="submit"]').before(
            `<div id="flag-form">
                    <label for="">Login</label><br>
                    <input type="text" name="login" placeholder="Login" value="${login}"><br>
                    <label for="">Password</label><br>
                    <input type="text" name="password" placeholder="Password"><br>
                </div>`
        );
    }

    //KICK FIX END

    openUserUpdateModal(id, firstName, lastName);
}

function deleteUser(ref, id) {
    if (!confirm("You are about to delete a user. Do you wanna proceed ?")) {
        return;
    }

    $.ajax({
        method: 'DELETE',
        url: '/api/users/' + id,
        success: function (result) {
            if (result.success) {
                $(ref).closest('tr').remove();
                alert(result.message);
            } else {
                alert(result.error.message);
            }
        },
        error: function () {
            alert('An unknown error occurred. Please retry later.');
        }
    });

}

function openUserUpdateModal(id, firstName, lastName) {
    const formRef = $('#update-user-form');

    $(formRef).find('input[name="firstName"]').val(firstName);
    $(formRef).find('input[name="lastName"]').val(lastName);
    $(formRef).find('input[name="id"]').val(id);

    $(formRef).show();
    $('#layer').css('display', 'flex');
}

function openUserCreationModal() {
    $('#add-user-form').show();
    $('#layer').css('display', 'flex');
}

function hideLayer() {
    const layerRef = $('#layer');

    $(layerRef).children().hide();
    $(layerRef).hide();
}

function appendUser(user) {
    $('#users-table tbody').append($(
        `<tr id="user-${user.id}">
            <td>${user.id}</td>
            <td>${user.firstName}</td>
            <td>${user.lastName}</td>
            <td>${user.registrationDate}</td>
            <td>${user.login}</td>
            <td><button onclick="editUser(this, ${user.id})">Edit</button></td>
            <td><button onclick="deleteUser(this, ${user.id})">Delete</button></td>
        </tr>`
        )
    )
}