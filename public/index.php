<?php


/**
 * @author Ibrahima Ndiaye
 */
use App\Http\Router;

require_once __DIR__ . '/../bootstrap/app.php';

Router::get('/login', 'AppController@login');
Router::get('/home', 'AppController@home');
Router::get('/logout', 'AppController@logout');

//USERS API SECTION
Router::get('/api/users', 'UserController@getAll');
Router::post('/api/users/login', 'UserController@login');
Router::delete('/api/users/{:i}', 'UserController@delete');
Router::put('/api/users/{:i}', 'UserController@update');
Router::post('/api/users', 'UserController@create');

Router::handle();