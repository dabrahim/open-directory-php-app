<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Login Page</title>
        <script type="application/javascript" src="/vendor/js/jquery-3.4.1.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <h1>Login</h1>
            <form action="" method="post" id="login-form">
                <input type="text" name="login" placeholder="Login"><br>
                <input type="password" name="password" placeholder="Password"><br>
                <button>Login</button>
            </form>
        </div>
        <script type="application/javascript" src="/dist/js/login.js"></script>
    </body>
</html>