<?php

use App\Persistence\Model\User;

/** @var User $user */
/** @var $users [] */

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Home</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/dist/css/home.min.css">
        <link rel="stylesheet" href="/vendor/css/animate.css">
        <script type="application/javascript" src="/vendor/js/jquery-3.4.1.js"></script>
        <script type="application/javascript" src="/dist/js/home-functions.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <nav>
                <h1>Hello <?= $user->getFirstName(); ?> <?= $user->getLastName(); ?></h1>
                <button onclick="openUserCreationModal();">Add a new user</button>
                <a href="/logout">
                    <button>Logout</button>
                </a>
            </nav>
            <div id="below-nav">
                <table id="users-table">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Registration Date</th>
                        <th>Login</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $usr): ?>
                        <tr id="user-<?= $usr['id']; ?>">
                            <td><?= $usr['id']; ?></td>
                            <td><?= $usr['firstName']; ?></td>
                            <td><?= $usr['lastName']; ?></td>
                            <td><?= $usr['registrationDate']; ?></td>
                            <td><?= $usr['login']; ?></td>
                            <td>
                                <button onclick="editUser(this, <?= $usr['id']; ?>)">Edit</button>
                            </td>
                            <td>
                                <button onclick="deleteUser(this, <?= $usr['id']; ?>)">Delete</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="layer" class="animated fadeIn">
            <form action="" method="post" id="add-user-form" class="animated fadeInLeft">
                <h1>Adding a new user</h1>
                <input type="text" name="firstName" placeholder="First Name"><br>
                <input type="text" name="lastName" placeholder="Last Name"><br>
                <input type="text" name="login" placeholder="Login"><br>
                <button type="submit">Add User</button>
            </form>

            <form action="" method="post" id="update-user-form" class="animated fadeInLeft">
                <h1>Updating a user</h1>
                <label for="">First Name</label><br>
                <input type="text" name="firstName" placeholder="First Name"><br>
                <input type="number" name="id" style="display: none;">
                <label for="">Last Name</label><br>
                <input type="text" name="lastName" placeholder="Last Name"><br>
                <button type="submit">Update User</button>
            </form>

        </div>
        <div id="flag" data-id="<?= $user->getId(); ?>"></div>
        <script type="application/javascript" src="/dist/js/home.js"></script>
    </body>
</html>