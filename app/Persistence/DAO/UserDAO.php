<?php


namespace App\Persistence\DAO;


use App\Persistence\Model\User;

interface UserDAO {

    function login($login, $password);
    function create(User $user);
    function getAll();
    function delete($userId);
    function update(User $user);
}