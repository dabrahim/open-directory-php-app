<?php


namespace App\Persistence\Util;


use SoapClient;
use SoapFault;

class SoapClientWrapper {

    private static $_USER_SOAP_CLIENT_INSTANCE;
    private static $_TOKEN_SOAP_CLIENT_INSTANCE;

    private function __construct() {
    }

    /**
     * @return SoapClient
     * @throws SoapFault
     */
    public static function getUserSoapClientInstance() {

        if (self::$_USER_SOAP_CLIENT_INSTANCE == null) {
            self::$_USER_SOAP_CLIENT_INSTANCE = new SoapClient($_ENV['USER_WS_WSDL']);
        }

        return self::$_USER_SOAP_CLIENT_INSTANCE;
    }

    /**
     * @return SoapClient
     * @throws SoapFault
     */
    public static function getUserTokenClientInstance() {

        if (self::$_TOKEN_SOAP_CLIENT_INSTANCE == null) {
            self::$_TOKEN_SOAP_CLIENT_INSTANCE = new SoapClient($_ENV['TOKEN_WS_WSDL']);
        }

        return self::$_TOKEN_SOAP_CLIENT_INSTANCE;
    }
}