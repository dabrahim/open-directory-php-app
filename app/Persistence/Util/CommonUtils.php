<?php


namespace App\Persistence\Util;


class CommonUtils {

    public static function stdToArray($input) {
        return json_decode(json_encode($input), true);
    }
}