<?php


namespace App\Persistence\Util;


use App\Persistence\Model\User;
use SoapFault;


/**
 * @author Ibrahima Ndiaye
 */
class SessionManager {
    private $_soapClient;
    private $_token;
    private $_cookie_name;
    private $_sessionData;
    public static $_USER_ID_KEY = 'id';
    public static $_USER_FIRST_NAME_KEY = 'firstName';
    public static $_USER_LAST_NAME_KEY = 'lastName';

    /**
     * SessionManager constructor.
     * @throws SoapFault
     */
    function __construct() {
        $this->_cookie_name = $_ENV['SESSION_COOKIE_NAME'];
        $this->_soapClient = SoapClientWrapper::getUserTokenClientInstance();
        $this->_token = $_COOKIE[$this->_cookie_name];
        $this->_sessionData = $this->getSessionData();
    }

    public function start($token, $session_duration_in_days = 3) {
        if ($this->isValidToken($token)) {
            setcookie($this->_cookie_name, $token, time() + 3600 * 24 * $session_duration_in_days, "/");
        }
    }

    public function destroy() {
        //send remote request
        //unset($_COOKIE[$this->_cookie_name]);
        setcookie($this->_cookie_name, "", time() - 3600, "/");
    }

    public function get($key) {
        $value = null;
        if ($this->_sessionData !== null && isset($this->_sessionData->$key)) {
            $value = $this->_sessionData->$key;
        }
        return $value;
    }

    public function isConnected() {
        return isset($_COOKIE[$this->_cookie_name])
            && $this->isValidToken($this->_token);
    }

    private function getSessionData() {
        $response = $this->_soapClient->retrieveTokenUser([
            'tokenValue' => $this->_token
        ]);
        return $response->return;
    }

    private function isValidToken($token) {
        return $this->_soapClient->retrieveTokenUser([
                'tokenValue' => $token
            ])->return !== null;
    }

    public function getUserObject() {
        $user = null;

        if ($this->isConnected()) {
            $user = new User();
            $user->setFirstName($this->get(self::$_USER_FIRST_NAME_KEY));
            $user->setLastName($this->get(self::$_USER_LAST_NAME_KEY));
            $user->setId($this->get(self::$_USER_ID_KEY));
        }

        return $user;
    }
}