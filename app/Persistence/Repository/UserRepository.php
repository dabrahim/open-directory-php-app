<?php


namespace App\Persistence\Repository;


use App\Persistence\DAO\UserDAO;
use App\Persistence\Model\User;
use App\Persistence\Util\CommonUtils;
use App\Persistence\Util\SoapClientWrapper;
use SoapFault;
use stdClass;


/**
 * @author Ibrahima Ndiaye
 */
class UserRepository implements UserDAO {

    private $_soapClient;

    /**
     * UserRepository constructor.
     * @throws SoapFault
     */
    public function __construct() {
        $this->_soapClient = SoapClientWrapper::getUserSoapClientInstance();
    }

    /**
     * @param $login
     * @param $password
     * @return mixed : session token or null
     */
    function login($login, $password) {
        $response = $this->_soapClient->login([
            'login' => $login,
            'password' => $password
        ]);

        return $response->return->value;
    }

    function create(User $user) {
        $soapData = new stdClass();
        $soapData->firstName = $user->getFirstName();
        $soapData->lastName = $user->getLastName();
        $soapData->login = $user->getLogin();

        $response = $this->_soapClient->add([
            'user' => $soapData
        ]);

        return CommonUtils::stdToArray($response->return);
    }

    function getAll() {
        $result = $this->_soapClient->getAll();
        return CommonUtils::stdToArray($result->return);
    }

    function delete($userId) {
        try {
            $this->_soapClient->delete([
                'userId' => $userId
            ]);

            $success = true;

        } catch (SoapFault $e) {
            $success = false;
        }

        return $success;
    }

    function update(User $user) {
        $res = $this->_soapClient->findById([
            'userId' => $user->getId()
        ]);

        $usr = $res->return;

        $soapData = new stdClass();
        $soapData->firstName = $user->getFirstName() ?: $usr->firstName;
        $soapData->lastName = $user->getLastName() ?: $usr->lastName;
        $soapData->id = $user->getId();
        $soapData->login = $user->getLogin() ?: $usr->login;
        $soapData->password = $user->getPassword() ?: $usr->password;

        $response = $this->_soapClient->update([
            'user' => $soapData
        ]);

        return CommonUtils::stdToArray($response->return);
    }

}