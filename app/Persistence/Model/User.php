<?php


namespace App\Persistence\Model;


class User {
    use Hydratable;

    private $_firstName;
    private $_lastName;
    private $_login;
    private $_password;
    private $_id;

    public function __construct() {
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        return $this->_firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName) {
        $this->_firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return $this->_lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName) {
        $this->_lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getLogin() {
        return $this->_login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login) {
        $this->_login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword() {
        return $this->_password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password) {
        $this->_password = $password;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->_id = $id;
    }
}