<?php


namespace App\Persistence\Model;


trait Hydratable {
    public function hydrate(array $params) {
        foreach ($params as $key => $val) {
            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($val);
            }
        }
    }
}