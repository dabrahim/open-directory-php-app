<?php


namespace App\Http;

/**
 * @author Ibrahima Ndiaye
 */
class Router {
    private static $CONTROLLER_REGEX = "/^([A-Z]{1}[a-zA-Z]+Controller)@([a-z]{1}[a-zA-Z]+)$/";
    private static $PATH_REGEX = '#^\/?[a-zA-Z0-9]+(\/(([a-zA-Z0-9]+)|({:[i]})))*\/?$#';
    private static $WILDCARDS = array(
        '{:i}' => '([0-9]+)'
    );
    private static $WILDCARD_SYNTAX = '#(\/{:[i]})#';
    private static $GET_ENDPOINTS = [];
    private static $POST_ENDPOINTS = [];
    private static $DELETE_ENDPOINTS = [];
    private static $PUT_ENDPOINTS = [];
    private static $CONTROLLERS_NAMESPACE = "App\Http\Controllers\\";

    public static function get($uri, $handler) {
        self::$GET_ENDPOINTS [] = self::processParams($uri, $handler);
    }

    public static function put($uri, $handler) {
        self::$PUT_ENDPOINTS [] = self::processParams($uri, $handler);
    }

    public static function delete($uri, $handler) {
        self::$DELETE_ENDPOINTS [] = self::processParams($uri, $handler);
    }

    public static function post($uri, $handler) {
        self::$POST_ENDPOINTS[] = self::processParams($uri, $handler);
    }

    private static function processParams($uri, $handler) {
        if (!preg_match(self::$PATH_REGEX, $uri)) {
            throw new \RuntimeException("The specified $uri is invalid !");
        }

        if (preg_match(self::$CONTROLLER_REGEX, $handler, $matches)) {
            $className = $matches[1];
            $methodName = $matches[2];

        } else {
            throw new \RuntimeException("Invalid handler syntax");
        }

        return array(
            'uri' => $uri,
            'className' => $className,
            'methodName' => $methodName
        );
    }

    public static function handle() {
        $uri = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];

        switch ($method) {
            case 'GET':
                self::handleGetRequest($uri);
                break;
            case 'POST':
                self::handlePostRequest($uri);
                break;
            case 'PUT':
                self::handlePutRequest($uri);
                break;
            case 'DELETE':
                self::handleDeleteRequest($uri);
                break;
            default:
                self::unsupportedHttpVerb();
                break;
        }
    }

    private static function handleRequest(array $handlers, $request_uri) {
        $patterns = [];
        $replacements = [];

        foreach (self::$WILDCARDS as $wc => $rgx) {
            $patterns[] = "#$wc#";
            $replacements[] = $rgx;
        }

        foreach ($handlers as $endpoint) {
            $defined_uri = $endpoint['uri'];
            $requested_uri_was_defined = false;
            $uri_with_wild_card = false;

            //We check whether there's a wildcard or not.
            //If yes then we replace it with the matching
            //regex
            $arguments = [];

            if (preg_match(self::$WILDCARD_SYNTAX, $defined_uri)) {
                $uri_with_wild_card = true;
                $uri_pattern = preg_replace($patterns, $replacements, $defined_uri);
                $requested_uri_was_defined = preg_match("#$uri_pattern#", $request_uri, $arguments);
            }

            if (!$uri_with_wild_card) {
                $requested_uri_was_defined = ($defined_uri == $request_uri);
            }

            array_shift($arguments);

            if ($requested_uri_was_defined) {
                $className = self::$CONTROLLERS_NAMESPACE . $endpoint['className'];
                $methodName = $endpoint['methodName'];

                if (class_exists($className)) {
                    //call_user_func(array($className, $methodName), ...$matches); //does a static call
                    (new $className())->$methodName(...$arguments);

                } else {
                    throw new \RuntimeException("This specified class doesn't exist !");
                }

                return;
            }
        }

        self::pageNotFound();
    }

    private static function handleGetRequest($uri) {
        self::handleRequest(self::$GET_ENDPOINTS, $uri);
    }

    private static function handlePutRequest($uri) {
        self::handleRequest(self::$PUT_ENDPOINTS, $uri);
    }

    private static function handleDeleteRequest($uri) {
        self::handleRequest(self::$DELETE_ENDPOINTS, $uri);
    }

    private static function handlePostRequest($uri) {
        self::handleRequest(self::$POST_ENDPOINTS, $uri);
    }

    private static function pageNotFound() {
        echo "Requested page not found";
    }

    private static function unsupportedHttpVerb() {
        echo "This HTTP method is not supported";
    }
}