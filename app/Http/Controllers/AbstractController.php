<?php


namespace App\Http\Controllers;

/**
 * @author Ibrahima Ndiaye
 */
abstract class AbstractController {
    private static $VIEW_NAME_REGEX = "/^([a-zA-Z]+)(_view)?(.php)?$/";

    protected function load_view($name, $vars = array()) {

        if (preg_match(self::$VIEW_NAME_REGEX, $name, $matches)) {
            $path = __DIR__ . "/../../../views/" . $matches[1] . "_view.php";

            if (file_exists($path)) {
                extract($vars);
                require_once $path;

            } else {
                throw new \RuntimeException("The specified view $name doesn't exist");
            }

        } else {
            throw new \RuntimeException("Invalid view syntax. Must be name, name_view or name_view.php");
        }
    }

    protected static function json($data) {
        header("Content-Type: application/json");
        echo json_encode($data);
    }

    protected function redirect($url) {
        header("Location: $url");
    }
}