<?php


namespace App\Http\Controllers;


/**
 * @author Ibrahima Ndiaye
 */
use App\Persistence\Model\User;
use App\Persistence\Repository\UserRepository;
use App\Persistence\Util\SessionManager;

class UserController extends AbstractController {
    private $_userRepo;

    public function __construct() {
        $this->_userRepo = new UserRepository();
    }

    public function login() {
        $login = $_POST['login'];
        $password = $_POST['password'];

        $token = $this->_userRepo->login($login, $password);

        $result = [
            'success' => false
        ];

        if ($token !== null) {
            $sm = new SessionManager();
            $sm->start($token);

            $result['success'] = true;
            $result['message'] = 'Login successful';
            $result['redirectTo'] = '/home';

        } else {
            $result['error']['message'] = 'Login or password incorrect';
        }

        self::json($result);
    }

    public function create() {
        $user = new User();
        $user->hydrate($_POST);

        $data = $this->_userRepo->create($user);

        $result = [
            'success' => true,
            'data' => $data
        ];

        self::json($result);
    }

    public function update($id) {
        parse_str(file_get_contents('php://input'), $_PUT);

        $user = new User();
        $user->hydrate($_PUT);
        $user->setId($id);

        $response = $this->_userRepo->update($user);

        self::json($response);
    }

    public function delete($userId) {
        $result = [];

        if ($this->_userRepo->delete($userId)) {
            $result['success'] = true;
            $result['message'] = "The user was successfully deleted !";

        } else {
            $result['success'] = false;
            $result['error']['message'] = "An error occurred while deleting the user. Please retry later";
        }

        self::json($result);
    }

    public function getAll() {
        $users = $this->_userRepo->getAll();
        self::json($users);
    }
}