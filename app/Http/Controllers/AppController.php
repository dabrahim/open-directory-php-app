<?php


namespace App\Http\Controllers;


use App\Persistence\Repository\UserRepository;
use App\Persistence\Util\SessionManager;

/**
 * @author Ibrahima Ndiaye
 */
class AppController extends AbstractController {
    private $_sessionManager;
    private $_userRepo;

    public function __construct() {
        $this->_sessionManager = new SessionManager();
        $this->_userRepo = new UserRepository();
    }

    public function login() {
        if ($this->_sessionManager->isConnected()) {
            $this->redirect("/home");
        }

        $this->load_view('login_view');
    }

    public function logout() {
        $this->_sessionManager->destroy();
        $this->redirect("/login");
    }

    public function home() {
        //We make sure the user is connected
        if (!$this->_sessionManager->isConnected()) {
            $this->redirect("/login");
        }

        $user = $this->_sessionManager->getUserObject();
        $users = $this->_userRepo->getAll();

        //then we return the view
        $this->load_view('home', array(
            'user' => $user,
            'users' => $users
        ));
    }
}